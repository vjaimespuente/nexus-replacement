FROM docker/compose:alpine-1.27.4

RUN apk add git && apk update && echo "GIT INSTALLED"
RUN  apk add docker && echo "INSTALLED DOCKER"
RUN  apk update && echo "UPDATE"
RUN  apk add openrc --no-cache 
RUN  rc-service docker start "DOCKER START"
RUN echo "rerun4"
RUN git clone https://github.com/victor-jaimes-puente/proget-docker-compose.git && echo "CLONE COMPLETE"
WORKDIR /proget-docker-compose 
RUN  apk add docker-compose
RUN  docker-compose up -d db "DB UP"
RUN  docker exec -it proget-sql /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "dapper123" -Q "CREATE DATABASE [ProGet] COLLATE SQL_Latin1_General_CP1_CI_AS" && echo "DATABASE UP"
RUN docker-compose up -d && "FINISHED"

# RUN set -ex && apk --no-cache add sudo && echo "ADD SUDO" 
# RUN apk add git && apk update && echo "GIT INSTALLED"
# RUN  apk add docker && echo "INSTALLED DOCKER"
# RUN  apk update && echo "UPDATE!"
# RUN  apk add openrc --no-cache 
# RUN rc-update add docker boot
# # RUN service docker start
# # RUN /etc/init.d/docker restart
# RUN apk --no-cache add curl && echo "ADD CURL"
# RUN sudo curl -L --fail https://github.com/docker/compose/releases/download/1.27.4/run.sh -o /usr/local/bin/docker-compose
# RUN sudo chmod +x /usr/local/bin/docker-compose && echo "INSTALLING THE DOCKER_COMPOSE"
# # RUN  rc-service docker start "DOCKER START"
# RUN echo "rerun4"
# RUN git clone https://github.com/victor-jaimes-puente/proget-docker-compose.git && echo "CLONE COMPLETE"
# WORKDIR /proget-docker-compose 
# # RUN  docker-compose up -d db && echo "DB UP"
# # RUN  docker exec -it proget-sql /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "dapper123" -Q "CREATE DATABASE [ProGet] COLLATE SQL_Latin1_General_CP1_CI_AS" && echo "DATABASE UP"
