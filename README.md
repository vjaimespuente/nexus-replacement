# nexus-replacement

---------For Magneto--------

STEPS:

Create and save your docker-compose.yml (see our example compose file).
In a terminal, navigate to the folder you saved your docker-compose.yml to.


Start the database image using docker-compose up -d db.


Create the ProGet database using the Docker command.

    docker exec -it proget-sql /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "<password>" -Q "CREATE DATABASE [ProGet] COLLATE SQL_Latin1_General_CP1_CI_AS"

Start the remaining images using 

docker-compose up -d

----------- For Magneto -----------